import { ISize } from '@interfaces';

export const SIZES: ISize[] = [
    { width: 300, height: 200, name: 'small' },
    { width: 600, height: 400, name: 'medium' },
    { width: 900, height: 600, name: 'large' }
]

export const INNER_SIZE: ISize = {
    width: 1200,
    height: 800
}
