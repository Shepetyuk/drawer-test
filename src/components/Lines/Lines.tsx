import React from 'react';
import { ILine } from '@interfaces';
import './Lines.scss';

interface LinesProps {
    lines: ILine[];
}

const Lines: React.FC<LinesProps> = ({ lines }) => {
    return (
        <div className="lines">
            {
                lines.map((line, index) => (
                    <span className="lines__item" key={ index }>
                        { `Line ${index + 1} - points [${ line.points.join(', ') }]` }
                    </span>
                ))
            }
        </div>
    );
};

export default Lines;
