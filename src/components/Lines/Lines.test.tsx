import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Lines from './Lines';
import { ILine } from '@interfaces';

describe('Lines Component', () => {
    const testLines: ILine[] = [
        { points: [0, 0, 100, 0] },
        { points: [0, 0, 0, 100] }
    ];

    it('should render component with lines', () => {
        render(<Lines lines={ testLines } />);

        const lineItems = screen.getAllByText(/Line \d+ - points \[.*\]/);

        expect(lineItems.length).toBe(2);
        expect(screen.getByText('Line 1 - points [0, 0, 100, 0]')).toBeInTheDocument();
        expect(screen.getByText('Line 2 - points [0, 0, 0, 100]')).toBeInTheDocument();
    });

    it('should render component without lines', () => {
        render(<Lines lines={ [] } />);

        const lineItems = screen.queryAllByText(/Line \d+ - points \[.*\]/);

        expect(lineItems.length).toBe(0);
    });
});
