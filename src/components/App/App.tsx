import React, { useState } from 'react';
import Canvas from '@components/Canvas/Canvas';
import ChangeSize from '@components/ChangeSize/ChangeSize';
import Lines from '@components/Lines/Lines';
import { SIZES } from '@constants';
import { ILine } from '@interfaces';
import './App.scss';

const App: React.FC = () => {
    const [size, setSize] = useState(SIZES[1])
    const [lines, setLines] = useState<ILine[]>([]);

    const addLine = (line: ILine) => {
        setLines([...lines, line]);
    };

    return (
        <div className="app">
            <ChangeSize setSize={ setSize } activeSize={ size }/>
            <Canvas size={ size } lines={ lines } addLine={ addLine }/>
            {
                lines.length ? (
                    <Lines lines={ lines }/>
                ) : null
            }
        </div>
    );
};

export default App;
