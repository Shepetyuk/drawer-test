import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import ChangeSize from './ChangeSize';
import { SIZES } from '@constants';
import { ISize } from '@interfaces';

describe('ChangeSize Component', () => {
    const mockSetSize = jest.fn();
    const activeSize: ISize = SIZES[1];

    beforeEach(() => {
        mockSetSize.mockClear();
        render(<ChangeSize setSize={ mockSetSize } activeSize={ activeSize }/>);
    });

    it('should render all size buttons', () => {
        SIZES.forEach(size => {
            const name = size.name as string;

            expect(screen.getByText(name)).toBeInTheDocument();
        });
    });

    it('should highlight active size button', () => {
        const activeName = activeSize.name as string;
        const activeButton = screen.getByText(activeName);

        expect(activeButton).toHaveClass('change-size__item--active');
    });

    it('should call setSize with the correct size when a button is clicked', () => {
        SIZES.forEach(size => {
            const name = size.name as string;
            const button = screen.getByText(name);

            fireEvent.click(button);

            expect(mockSetSize).toHaveBeenCalledWith(size);
        });
    });
});
