import React from 'react';
import { SIZES } from '@constants';
import { ISize } from '@interfaces';
import './ChangeSize.scss';

interface ChangeSizeProps {
    setSize: (size: ISize) => void;
    activeSize: ISize;
}

const ChangeSize: React.FC<ChangeSizeProps> = ({ setSize, activeSize }) => {
    return (
        <div className="change-size">
            {
                SIZES.map((size: ISize, index) => {
                    const activeClass: string = size.name === activeSize.name ? 'change-size__item--active': ''

                    return (
                        <button
                            className={ `change-size__item ${activeClass}` }
                            key={ index }
                            onClick={ () => setSize(size) }
                        >
                            { size.name }
                        </button>
                    )
                })
            }
        </div>
    );
};

export default ChangeSize;
