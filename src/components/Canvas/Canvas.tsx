import React, { useRef, useState, useEffect } from 'react';
import { ILine, ISize } from '@interfaces';
import { INNER_SIZE } from '@constants';
import './Canvas.scss';

interface CanvasProps {
    size: ISize;
    lines: ILine[];
    addLine: (line: ILine) => void;
}

const Canvas: React.FC<CanvasProps> = ({ size, lines, addLine }) => {
    const canvasRef = useRef<HTMLCanvasElement>(null);
    const [isDrawing, setIsDrawing] = useState(false);
    const [startX, setStartX] = useState(0);
    const [startY, setStartY] = useState(0);
    const scale = Math.min(size.width / INNER_SIZE.width, size.height / INNER_SIZE.height);

    useEffect(() => {
        const canvas = canvasRef.current;

        if (canvas) {
            const ctx = canvas.getContext('2d');

            if (ctx) {
                drawLines(ctx);
            }
        }
    }, [lines, size]);

    const getMousePosition = (e: React.MouseEvent) => {
        const rect = canvasRef.current?.getBoundingClientRect();
        const scaleX = INNER_SIZE.width / (rect?.width || 1);
        const scaleY = INNER_SIZE.height / (rect?.height || 1);

        return {
            x: (e.clientX - (rect?.left || 0)) * scaleX,
            y: (e.clientY - (rect?.top || 0)) * scaleY
        };
    };

    const drawLines = (ctx: CanvasRenderingContext2D) => {
        ctx.clearRect(0, 0, canvasRef.current!.width, canvasRef.current!.height);
        ctx.lineWidth = 2 / scale;

        lines.forEach(line => {
            ctx.beginPath();
            ctx.moveTo(line.points[0], line.points[1]);
            ctx.lineTo(line.points[2], line.points[3]);
            ctx.stroke();
        });
    };

    const startDrawing = (e: React.MouseEvent) => {
        const { x, y } = getMousePosition(e);

        setStartX(x);
        setStartY(y);
        setIsDrawing(true);
    };

    const draw = (e: React.MouseEvent) => {
        if (!isDrawing) return;

        const { x, y } = getMousePosition(e);
        const canvas = canvasRef.current;
        const dx = Math.abs(x - startX);
        const dy = Math.abs(y - startY);

        if (canvas) {
            const ctx = canvas.getContext('2d');

            if (ctx) {
                drawLines(ctx);

                ctx.moveTo(startX, startY);
                dx > dy ? ctx.lineTo(x, startY) : ctx.lineTo(startX, y);
                ctx.stroke();
            }
        }
    };

    const endDrawing = (e: React.MouseEvent) => {
        if (!isDrawing) return;

        const { x, y } = getMousePosition(e);
        const line: ILine = {
            points: [startX, startY, x, y]
        };

        if (Math.abs(x - startX) > Math.abs(y - startY)) {
            line.points[3] = startY;
        } else {
            line.points[2] = startX;
        }

        addLine(line);
        setIsDrawing(false);
    };

    return (
        <canvas
            ref={ canvasRef }
            className="canvas"
            role="img"
            width={ INNER_SIZE.width }
            height={ INNER_SIZE.height }
            style={{ ...size }}
            onMouseDown={ startDrawing }
            onMouseUp={ endDrawing }
            onMouseMove={ draw }
        />
    );
};

export default Canvas;
