import React from 'react';
import { render, screen } from '@testing-library/react';
import Canvas from './Canvas';
import { ILine, ISize } from '@interfaces';
import { setupJestCanvasMock } from 'jest-canvas-mock';
import { SIZES } from '@constants';

describe('Canvas Component', () => {
    const mockAddLine = jest.fn();
    const size: ISize = SIZES[2];
    const lines: ILine[] = [
        { points: [1, 1, 100, 1] },
        { points: [1, 1, 1, 100] }
    ];

    beforeEach(() => {
        mockAddLine.mockClear();
        setupJestCanvasMock();
        render(<Canvas size={ size } lines={ lines } addLine={ mockAddLine }/>);
    });

    it('should render component', () => {
        const canvas = screen.getByRole('img');

        expect(canvas).toBeInTheDocument();
        expect(canvas).toHaveStyle({ width: `${ size.width}px`, height: `${size.height}px` });
    });

    it('should render lines', () => {
        const canvas = screen.getByRole('img') as HTMLCanvasElement;
        const context = canvas.getContext('2d');

        if (context) {
            expect(context.__getDrawCalls().filter(call => call.type === 'stroke')).toHaveLength(lines.length);
        }
    });
});
