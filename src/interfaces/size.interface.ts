export interface ISize {
    width: number;
    height: number;
    name?: string;
}
